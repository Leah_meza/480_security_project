#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	/* NOTE: len may overestimate the number of bytes actually required. */
	unsigned char* buf = malloc(len);
	Z2BYTES(buf,len,x);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);
	/* TODO: write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */
	size_t len = keyBits / 8; //get bytes
	unsigned char* raw_bytes = malloc(len); //buffer of byte length
	//for loop checking primality of p
	while(!ISPRIME(K->p))
	{
		randBytes(raw_bytes, len);
		BYTES2Z(K->p, raw_bytes, len);
	}
	free(raw_bytes);
	//for loop checking the primality of q
	while(!ISPRIME(K->q) && mpz_cmp(K->p, K->q))
	{
		randBytes(raw_bytes, len);
		BYTES2Z(K->q, raw_bytes, len);
	}
	free(raw_bytes);
	//found n=p*q
	mpz_mul(K->n, K->p, K->q);
	//encryption exponent
    mpz_t p_;    mpz_init(p_);
	mpz_t q_;    mpz_init(q_);
	mpz_t exp;   mpz_init(exp);
	mpz_t one;   mpz_set_ui(&one,1);
	mpz_t t;	 mpz_init(t);
    mpz_sub_ui(p_, K->p, 1); //subtract int 1
    mpz_sub_ui(q_, K->q, 1); //subtract int 1
    mpz_mul(t, p_, q_);   //toit=(p-1)(q-1)
	//satisfy gcd((p-1)(q-1), d)=1
	unsigned char* d = malloc(len);
    while (mpz_cmp(exp, one)) //compare to 1
	{
        randBytes(d,len); 
        BYTES2Z(K->d, d, len); //d is a rand int
		mpz_gcd(exp, K->d, t); //gcd of (p-1)*(q-1), d
    }
	free(d);
	//mpz_invert is used to calculate the modular multiplicative inverse of a mod b
	//finding e is easy
	mpz_invert(K->e, K->d, t);

	mpz_clear(p_);
	mpz_clear(q_);
	mpz_clear(exp);
	mpz_clear(one);
	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len, RSA_KEY* K)
{
	mpz_t z;    mpz_init(z);
	mpz_t newz; mpz_init(newz);
    BYTES2Z(z, inBuf, len); //in-bytes to integer - this assumes len param is correct
    mpz_powm(newz, z, K->e, K->n); //satisfies (z^e) modulo n = C
    Z2BYTES(outBuf, len, newz);  

	mpz_clear(z);
	mpz_clear(newz);
	return len;  
}
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len, RSA_KEY* K)
{
	mpz_t z;    mpz_init(z);
    mpz_t newz; mpz_init(newz);
    BYTES2Z(z, inBuf, len);
    mpz_powm(newz, z, K->d, K->n); //satisifes (z^d) modulo n = M
    Z2BYTES(outBuf, len, newz);
	mpz_clear(z);
	mpz_clear(newz);
	return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
